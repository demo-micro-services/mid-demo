package handler

import (
	"context"
	"fmt"

	"github.com/golang/glog"
	"github.com/maolinc/copier"
	demoClientPb "gitlab.com/demo-micro-services/protobuf/demo-service"
	pb "gitlab.com/demo-micro-services/protobuf/middleware/demo"
	"google.golang.org/grpc"
)

var (
	portCoreDemo = 50051
)

type demoAPIHandler struct {
	demoServiceClient demoClientPb.DemoServiceClient
}

func NewDemoAPIHandler() pb.DemoAPIServiceServer {
	conn, err := grpc.Dial(fmt.Sprintf("demo-service:%d", portCoreDemo), grpc.WithInsecure())
	if err != nil {
		glog.Fatalf("did not connect: %v", err)
	}
	return &demoAPIHandler{
		demoServiceClient: demoClientPb.NewDemoServiceClient(conn),
	}
}

func (h *demoAPIHandler) ListOrders(ctx context.Context, req *pb.ListOrdersRequest) (*pb.ListOrdersResponse, error) {
	glog.Infof("[ListOrders] req: %+v", req)
	reqToCore := &demoClientPb.ListOrdersRequest{PageIndex: req.GetPageIndex(), PageSize: req.GetPageSize()}
	resFromCore, err := h.demoServiceClient.ListOrders(ctx, reqToCore)
	if err != nil {
		glog.Errorf("[ListOrders] err: %s", err)
		return nil, err
	}
	res := &pb.ListOrdersResponse{}
	copier.Copy(res, resFromCore)
	return res, nil
}
