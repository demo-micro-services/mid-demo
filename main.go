package main

import (
	"flag"
	"fmt"
	"net"
	"os"

	"github.com/golang/glog"
	"gitlab.com/demo-micro-services/mid-demo/handler"
	pb "gitlab.com/demo-micro-services/protobuf/middleware/demo"
	"google.golang.org/grpc"
)

var (
	portMidDemo = 50050
)

func main() {
	os.Args = append(os.Args, "-logtostderr=true")
	flag.Parse()
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", portMidDemo))
	if err != nil {
		glog.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	pb.RegisterDemoAPIServiceServer(s, handler.NewDemoAPIHandler())
	glog.Infof("server listening at %v", lis.Addr())
	if err := s.Serve(lis); err != nil {
		glog.Fatalf("failed to serve: %v", err)
	}
}
